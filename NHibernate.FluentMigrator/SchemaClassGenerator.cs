﻿using System;
using System.IO;
using System.Text;

namespace NHibernate.FluentMigrator
{
    /// <summary>
    /// Class to create file with FluentMigrator migrations.
    /// </summary>
    public class SchemaClassGenerator
    {
        /// <summary>
        /// Class to generate commands.
        /// </summary>
        private readonly SchemaGenerator _schemaFluentMigrator;

        /// <summary>
        /// Placeholder for Up scripts.
        /// </summary>
        public string UpPlaceholder = "$UP$";

        /// <summary>
        /// Placeholder for Down scripts.
        /// </summary>
        public string DownPlaceholder = "$DOWN$";

        /// <summary>
        /// Placeholder for class name.
        /// </summary>
        public string ClassNamePlaceholder = "$CLASSNAME$";

        /// <summary>
        /// Placeholder for namespace.
        /// </summary>
        public string ClassNamespacePlaceholder = "$NAMESPACE$";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="schemaFluentMigrator">Class to generate commands.</param>
        public SchemaClassGenerator(SchemaGenerator schemaFluentMigrator)
        {
            _schemaFluentMigrator = schemaFluentMigrator;
        }

        /// <summary>
        /// Creating standard file template.
        /// </summary>
        private string CreateTemplateFile()
        {
            var fileTemplateBuilder = new StringBuilder();
            fileTemplateBuilder.AppendLine("using System;");
            fileTemplateBuilder.AppendLine("using System.IO;");
            fileTemplateBuilder.AppendLine("using FluentMigrator;");
            fileTemplateBuilder.AppendLine();
            fileTemplateBuilder.AppendFormat("namespace {0} {{{1}", ClassNamespacePlaceholder, Environment.NewLine);
            fileTemplateBuilder.AppendLine("[Migration(1)]");
            fileTemplateBuilder.AppendFormat("public class {0} : Migration {{{1}", ClassNamePlaceholder, Environment.NewLine);
            fileTemplateBuilder.AppendLine("public override void Up() {");
            fileTemplateBuilder.AppendLine(UpPlaceholder);
            fileTemplateBuilder.AppendLine("}");
            fileTemplateBuilder.AppendLine();
            fileTemplateBuilder.AppendLine("public override void Down(){");
            fileTemplateBuilder.AppendLine(DownPlaceholder);
            fileTemplateBuilder.AppendLine("}");
            fileTemplateBuilder.AppendLine("}");
            fileTemplateBuilder.AppendLine("}");

            return fileTemplateBuilder.ToString();
        }

        /// <summary>
        /// Save file.
        /// </summary>
        /// <param name="path">Path were file will be saved.</param>
        /// <param name="className">Class name.</param>
        /// <param name="classNamespace">Class namespace.</param>
        /// <returns>Path with file prefix where file was saved.</returns>
        public string SaveClassFile(string path, string className, string classNamespace)
        {
            return SaveClassFile(path, className, classNamespace, null, null);
        }

        /// <summary>
        /// Save file.
        /// </summary>
        /// <param name="path">Path were file will be saved.</param>
        /// <param name="className">Class name.</param>
        /// <param name="classNamespace">Class namespace.</param>
        /// <param name="fileNamePrefix">File name prefix.</param>
        /// <returns>Path with file prefix where file was saved.</returns> 
        public string SaveClassFile(string path, string className, string classNamespace, string fileNamePrefix)
        {
            return SaveClassFile(path, className, classNamespace, fileNamePrefix, null);
        }

        /// <summary>
        /// Save file.
        /// </summary>
        /// <param name="path">Path were file will be saved.</param>
        /// <param name="className">Class name.</param>
        /// <param name="classNamespace">Class namespace.</param>
        /// <param name="fileNamePrefix">File name prefix.</param>
        /// <param name="fileTemplate">File template.</param>
        /// <returns>Path with file prefix where file was saved.</returns>
        public string SaveClassFile(string path, string className, string classNamespace, string fileNamePrefix, string fileTemplate)
        {
            // We have to create file template.
            if (string.IsNullOrWhiteSpace(fileTemplate))
            {
                fileTemplate = CreateTemplateFile();
            }

            // Changing placeholders.
            fileTemplate = fileTemplate.Replace(UpPlaceholder, _schemaFluentMigrator.GetUp());
            fileTemplate = fileTemplate.Replace(DownPlaceholder, _schemaFluentMigrator.GetDown());
            fileTemplate = fileTemplate.Replace(ClassNamePlaceholder, className);
            fileTemplate = fileTemplate.Replace(ClassNamespacePlaceholder, classNamespace);

            // Creating file name prefix.
            if (string.IsNullOrWhiteSpace(fileNamePrefix))
            {
                fileNamePrefix = DateTime.Now.ToString("yyyyMMddHHmmss");
            }

            string fileName = string.Format("{0}_{1}.cs", fileNamePrefix, className);
            string combinedPath = Path.Combine(path, fileName);

            // Save migration to file.
            File.WriteAllText(combinedPath, fileTemplate);

            // Path with file prefix.
            return combinedPath;
        }
    }
}
