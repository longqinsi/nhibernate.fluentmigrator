﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using NHibernate.Cfg;
using NHibernate.Dialect.Schema;
using NHibernate.Engine;
using NHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Util;
using NHibernate.SqlTypes;

namespace NHibernate.FluentMigrator
{
    /// <summary>
    /// Class to create FluentMigrator scripts from NHibernate.
    /// </summary>
    public class SchemaGenerator
    {
        private readonly Dialect.Dialect _dialect;
        private readonly DatabaseMetadata _databaseMetadata;
        private readonly Mappings _mappings;
        private readonly IMapping _mapping;
        private readonly string _defaultSchema;
        private readonly string _defaultCatalog;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="configuration">NHibernate configuration.</param>
        public SchemaGenerator(Configuration configuration)
        {
            _dialect = Dialect.Dialect.GetDialect(configuration.Properties);
            var props = new Dictionary<string, string>(_dialect.DefaultProperties);

            foreach (var prop in configuration.Properties)
            {
                props[prop.Key] = prop.Value;
            }

            IConnectionHelper connectionHelper = new ManagedProviderConnectionHelper(props);
            connectionHelper.Prepare();

            DbConnection connection = connectionHelper.Connection;
            _databaseMetadata = new DatabaseMetadata(connection, _dialect);

            _defaultCatalog = PropertiesHelper.GetString(Cfg.Environment.DefaultCatalog, configuration.Properties, null);
            _defaultSchema = PropertiesHelper.GetString(Cfg.Environment.DefaultSchema, configuration.Properties, null);

            _mapping = configuration.BuildMapping();
            _mappings = configuration.CreateMappings(_dialect);
        }

        /// <summary>
        /// All commands to up database.
        /// </summary>
        public string GetUp()
        {
            var fileStringBuilder = new StringBuilder();

            // Table data.
            fileStringBuilder.Append(BuildUpTablesString());

            // Foreign key data.
            fileStringBuilder.Append(BuildUpForeignKeysString());

            // Index data.
            fileStringBuilder.Append(BuildUpIndexesString());

            // All generated commands.
            return fileStringBuilder.ToString();
        }

        /// <summary>
        /// All commands to down database.
        /// </summary>
        /// <returns></returns>
        public string GetDown()
        {
            var fileStringBuilder = new StringBuilder();

            // Index data.
            fileStringBuilder.Append(BuildDownIndexesString());

            // Foreign key data.
            fileStringBuilder.Append(BuildDownForeignKeysString());

            // Table data.
            fileStringBuilder.Append(BuildDownTablesString());

            // All generated commands.
            return fileStringBuilder.ToString();
        }

        /// <summary>
        /// Generate table data (up commands).
        /// </summary>
        /// <returns></returns>
        private string BuildUpTablesString()
        {
            var tableStringBuilder = new StringBuilder();
            foreach (Table table in _mappings.IterateTables)
            {
                if (table.IsPhysicalTable && IncludeAction(table.SchemaActions, SchemaAction.Update))
                {
                    ITableMetadata tableInfo = _databaseMetadata.GetTableMetadata(table.Name, table.Schema ?? _defaultSchema, table.Catalog ?? _defaultCatalog, table.IsQuoted);
                    if (tableInfo == null)
                    {
                        string createTable = CreateUpTable(table);
                        if (!string.IsNullOrWhiteSpace(createTable))
                        {
                            tableStringBuilder.Append(createTable);
                        }
                    }
                    else
                    {
                        string alterTable = AlterUpTable(table, tableInfo);
                        if (!string.IsNullOrWhiteSpace(alterTable))
                        {
                            tableStringBuilder.Append(alterTable);
                        }
                    }
                }
            }

            return tableStringBuilder.ToString();
        }

        /// <summary>
        /// Generate table data (down commands)
        /// </summary>
        /// <returns></returns>
        private string BuildDownTablesString()
        {
            var tableStringBuilder = new StringBuilder();
            foreach (Table table in _mappings.IterateTables)
            {
                if (table.IsPhysicalTable && IncludeAction(table.SchemaActions, SchemaAction.Update))
                {
                    ITableMetadata tableInfo = _databaseMetadata.GetTableMetadata(table.Name, table.Schema ?? _defaultSchema, table.Catalog ?? _defaultCatalog, table.IsQuoted);
                    if (tableInfo == null)
                    {
                        string createTable = CreateDownTable(table);
                        if (!string.IsNullOrWhiteSpace(createTable))
                        {
                            tableStringBuilder.Append(createTable);
                        }
                    }
                    else
                    {
                        string alterTable = AlterDownTable(table, tableInfo);
                        if (!string.IsNullOrWhiteSpace(alterTable))
                        {
                            tableStringBuilder.Append(alterTable);
                        }
                    }
                }
            }

            return tableStringBuilder.ToString();
        }

        /// <summary>
        /// Generate foreign keys data (up commands).
        /// </summary>
        /// <returns></returns>
        private string BuildUpForeignKeysString()
        {
            var buffer = new StringBuilder();

            foreach (Table table in _mappings.IterateTables)
            {
                if (table.IsPhysicalTable && IncludeAction(table.SchemaActions, SchemaAction.Update))
                {
                    ITableMetadata tableInfo = _databaseMetadata.GetTableMetadata(table.Name, table.Schema, table.Catalog, table.IsQuoted);
                    bool tableCommentWasAdded = false;

                    foreach (var fk in table.ForeignKeyIterator)
                    {
                        if (fk.HasPhysicalConstraint && IncludeAction(fk.ReferencedTable.SchemaActions, SchemaAction.Update))
                        {
                            if (tableInfo == null || (tableInfo.GetForeignKeyMetadata(fk.Name) == null && (tableInfo.GetIndexMetadata(fk.Name) == null)))
                            {
                                if (!tableCommentWasAdded)
                                {
                                    buffer.AppendFormat("{0}// Foreign keys for table: {1}.{0}", System.Environment.NewLine, table.Name);
                                    tableCommentWasAdded = true;
                                }

                                buffer.AppendFormat("Create.ForeignKey(\"{0}\")", fk.Name);
                                buffer.AppendFormat(".FromTable(\"{0}\")", fk.Table.Name);
                                buffer.AppendFormat(".InSchema(\"{0}\")", _defaultSchema);

                                var referencedColumnBuilder = new StringBuilder();
                                for (int i = 0; i < fk.Columns.Count; i++)
                                {
                                    Column referencedColumn = fk.Columns[i];
                                    if (i > 0)
                                    {
                                        referencedColumnBuilder.Append(",");
                                    }

                                    referencedColumnBuilder.AppendFormat("{0}", referencedColumn.Name);
                                }
                                buffer.AppendFormat(".ForeignColumns(\"{0}\")", referencedColumnBuilder);

                                buffer.AppendFormat(".ToTable(\"{0}\")", fk.ReferencedTable.Name);
                                buffer.AppendFormat(".InSchema(\"{0}\")", _defaultSchema);

                                var primaryColumnBuilder = new StringBuilder();
                                for (int i = 0; i < fk.ReferencedTable.PrimaryKey.Columns.Count; i++)
                                {
                                    Column primaryColumn = fk.ReferencedTable.PrimaryKey.Columns[i];
                                    if (i > 0)
                                    {
                                        primaryColumnBuilder.Append(",");
                                    }

                                    primaryColumnBuilder.AppendFormat("{0}", primaryColumn.Name);
                                }
                                buffer.AppendFormat(".PrimaryColumns(\"{0}\")", primaryColumnBuilder);

                                buffer.AppendLine(";");
                            }
                        }
                    }
                }
            }

            return buffer.ToString();
        }

        /// <summary>
        /// Generate foreign keys data (down commands).
        /// </summary>
        /// <returns></returns>
        private string BuildDownForeignKeysString()
        {
            var buffer = new StringBuilder();

            foreach (Table table in _mappings.IterateTables)
            {
                if (table.IsPhysicalTable && IncludeAction(table.SchemaActions, SchemaAction.Update))
                {
                    ITableMetadata tableInfo = _databaseMetadata.GetTableMetadata(table.Name, table.Schema, table.Catalog, table.IsQuoted);
                    bool tableCommentWasAdded = false;

                    foreach (var fk in table.ForeignKeyIterator)
                    {
                        if (fk.HasPhysicalConstraint && IncludeAction(fk.ReferencedTable.SchemaActions, SchemaAction.Update))
                        {
                            if (tableInfo == null || (tableInfo.GetForeignKeyMetadata(fk.Name) == null && (tableInfo.GetIndexMetadata(fk.Name) == null)))
                            {
                                if (!tableCommentWasAdded)
                                {
                                    buffer.AppendFormat("{0}// Foreign keys for table: {1}.{0}", System.Environment.NewLine, table.Name);
                                    tableCommentWasAdded = true;
                                }

                                buffer.AppendFormat("Delete.ForeignKey(\"{0}\").OnTable(\"{1}\").InSchema(\"{2}\");{3}", fk.Name, table.Name, _defaultSchema, System.Environment.NewLine);
                            }
                        }
                    }
                }
            }

            return buffer.ToString();
        }

        /// <summary>
        /// Generate index data (up commands).
        /// </summary>
        /// <returns></returns>
        private string BuildUpIndexesString()
        {
            var buffer = new StringBuilder();

            foreach (Table table in _mappings.IterateTables)
            {
                if (table.IsPhysicalTable && (table.SchemaActions & SchemaAction.Update) != SchemaAction.None)
                {
                    ITableMetadata tableInfo = _databaseMetadata.GetTableMetadata(table.Name, table.Schema, table.Catalog, table.IsQuoted);
                    bool indexCommentWasAdded = false;

                    foreach (var index in table.IndexIterator)
                    {
                        if (tableInfo == null || tableInfo.GetIndexMetadata(index.Name) == null)
                        {
                            // Dodajemy komentarz, dzięki któremu wiadomo do której tabeli są klucze dodawane.
                            if (!indexCommentWasAdded)
                            {
                                buffer.AppendFormat("{0}// Indexes for table: {1}.{0}", System.Environment.NewLine, table.Name);
                                indexCommentWasAdded = true;
                            }

                            buffer.AppendFormat("Create");
                            buffer.AppendFormat(".Index(\"{0}\")", index.Name);
                            buffer.AppendFormat(".OnTable(\"{0}\")", table.Name);
                            buffer.AppendFormat(".InSchema(\"{0}\")", _defaultSchema);

                            foreach (Column column in index.ColumnIterator)
                            {
                                buffer.AppendFormat(".OnColumn(\"{0}\").Ascending()", column.Name);
                            }

                            buffer.AppendLine(";");
                        }
                    }
                }
            }

            buffer.AppendLine();

            return buffer.ToString();
        }

        /// <summary>
        /// Generate index data (down commands).
        /// </summary>
        /// <returns></returns>
        private string BuildDownIndexesString()
        {
            var buffer = new StringBuilder();

            foreach (Table table in _mappings.IterateTables)
            {
                if (table.IsPhysicalTable && (table.SchemaActions & SchemaAction.Update) != SchemaAction.None)
                {
                    ITableMetadata tableInfo = _databaseMetadata.GetTableMetadata(table.Name, table.Schema, table.Catalog, table.IsQuoted);
                    bool indexCommentWasAdded = false;

                    foreach (var index in table.IndexIterator)
                    {
                        if (tableInfo == null || tableInfo.GetIndexMetadata(index.Name) == null)
                        {
                            // Dodajemy komentarz, dzięki któremu wiadomo do której tabeli są klucze dodawane.
                            if (!indexCommentWasAdded)
                            {
                                buffer.AppendFormat("{0}// Indexes for table: {1}.{0}", System.Environment.NewLine, table.Name);
                                indexCommentWasAdded = true;
                            }

                            buffer.AppendFormat("Delete.Index(\"{0}\").OnTable(\"{1}\").InSchema(\"{2}\");{3}", index.Name, table.Name, _defaultSchema, System.Environment.NewLine);
                        }
                    }
                }
            }

            return buffer.ToString();
        }

        /// <summary>
        /// Creating a table in FluentMigrator.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <returns></returns>
        private string CreateUpTable(Table table)
        {
            var buffer = new StringBuilder();

            // We have to search primary column.
            string columnPrimaryKeyName = null;
            if (table.HasPrimaryKey)
            {
                foreach (Column column in table.PrimaryKey.ColumnIterator)
                {
                    columnPrimaryKeyName = column.Name;
                }
            }

            buffer.AppendFormat("{0}// Table: {1}.{0}", System.Environment.NewLine, table.Name);

            // Create table command.
            buffer.AppendFormat("Create.Table(\"{0}\")", table.Name);
            buffer.AppendFormat(".InSchema(\"{0}\")", _defaultSchema);

            int count = 1;
            foreach (Column col in table.ColumnIterator)
            {
                // Create column.
                buffer.AppendFormat(".WithColumn(\"{0}\")", col.Name);

                // Type of the column.
                buffer.AppendFormat(".{0}", GetMigrationTypeForType(col));

                // If column is primary key.
                if (col.Name != null && col.Name.Equals(columnPrimaryKeyName, StringComparison.CurrentCulture))
                {
                    buffer.AppendFormat(".PrimaryKey().Identity()");
                }

                // If column have default value.
                if (!string.IsNullOrEmpty(col.DefaultValue))
                {
                    buffer.AppendFormat(".WithDefaultValue(\"({0})\")", col.DefaultValue);
                }

                // If column is nullable.
                buffer.Append(col.IsNullable ? ".Nullable()" : ".NotNullable()");

                // If column is unique.
                if (col.IsUnique)
                {
                    buffer.AppendFormat(".Unique()");
                }

                buffer.AppendLine(count < table.ColumnIterator.Count() ? string.Empty : ";");
                count++;
            }

            return buffer.ToString();
        }

        /// <summary>
        /// Deleting table command.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <returns></returns>
        private string CreateDownTable(Table table)
        {
            var buffer = new StringBuilder();

            // We add the comment.
            buffer.AppendFormat("{0}// Tabela: {1}.{0}", System.Environment.NewLine, table.Name);

            // Command that delete table..
            buffer.AppendFormat("Delete.Table(\"{0}\").InSchema(\"{1}\");{2}", table.Name, _defaultSchema, System.Environment.NewLine);

            return buffer.ToString();
        }

        /// <summary>
        /// Command to alter table.
        /// </summary>
        /// <param name="table">Table.</param>
        /// <param name="tableInfo">Information about the table.</param>
        /// <returns></returns>
        private string AlterUpTable(Table table, ITableMetadata tableInfo)
        {
            var buffer = new StringBuilder();

            bool tableCommentWasAdded = false;
            foreach (Column column in table.ColumnIterator)
            {
                IColumnMetadata columnInfo = tableInfo.GetColumnMetadata(column.Name);
                if (columnInfo != null)
                {
                    continue;
                }

                // We add the comment.
                if (!tableCommentWasAdded)
                {
                    buffer.AppendFormat("{0}// Table: {1}.{0}", System.Environment.NewLine, table.Name);
                    tableCommentWasAdded = true;
                }

                // Table name.
                buffer.AppendFormat("Alter.Table(\"{0}\").InSchema(\"{1}\")", table.Name, _defaultSchema);

                // Add column command.
                buffer.AppendFormat(".AddColumn(\"{0}\")", column.GetQuotedName(_dialect));

                // Type od column.
                buffer.AppendFormat(".{0}", GetMigrationTypeForType(column));

                // If column have default value.
                if (!string.IsNullOrEmpty(column.DefaultValue))
                {
                    buffer.AppendFormat(".WithDefaultValue(\"({0})\")", column.DefaultValue);
                }

                // If column is nullable.
                buffer.Append(column.IsNullable ? ".Nullable()" : ".NotNullable()");

                // If column is unique.
                if (column.IsUnique)
                {
                    buffer.AppendFormat(".Unique()");
                }

                buffer.AppendLine(";");
            }

            return buffer.ToString();
        }

        /// <summary>
        /// Command to delete columns.
        /// </summary>
        /// <param name="table">Table.</param>
        /// <param name="tableInfo">Information about table.</param>
        /// <returns></returns>
        private string AlterDownTable(Table table, ITableMetadata tableInfo)
        {
            var buffer = new StringBuilder();

            bool tableCommentWasAdded = false;
            foreach (Column column in table.ColumnIterator)
            {
                IColumnMetadata columnInfo = tableInfo.GetColumnMetadata(column.Name);
                if (columnInfo != null)
                {
                    continue;
                }

                // We add comment.
                if (!tableCommentWasAdded)
                {
                    buffer.AppendFormat("{0}// Table: {1}.{0}", System.Environment.NewLine, table.Name);
                    tableCommentWasAdded = true;
                }

                buffer.AppendFormat("Delete.Column(\"{0}\").FromTable(\"{1}\").InSchema(\"{2}\");{3}", column.Name, table.Name, _defaultSchema, System.Environment.NewLine);
            }

            return buffer.ToString();
        }

        /// <summary>
        /// Method to find column type.
        /// </summary>
        /// <param name="column">Column from NHibernate.</param>
        /// <returns></returns>
        private string GetMigrationTypeForType(Column column)
        {
            var size = column.Length;
            string sizeStr = ((size == -1) ? "" : size.ToString(CultureInfo.CurrentCulture));

            // Custom SQL type.
            if (!string.IsNullOrWhiteSpace(column.SqlType))
            {
                return string.Format("AsCustom(\"{0}\")", column.SqlType);
            }

            string sysType;

            SqlType sqlType = column.GetSqlTypeCode(_mapping);
            switch (sqlType.DbType)
            {
                case DbType.AnsiString:
                    sysType = string.Format("AsAnsiString({0})", sizeStr);
                    break;
                case DbType.AnsiStringFixedLength:
                    sysType = string.Format("AsFixedLengthAnsiString({0})", sizeStr);
                    break;
                case DbType.Binary:
                    sysType = string.Format("AsBinary({0})", size == -1 ? "Int32.MaxValue" : sizeStr);
                    break;
                case DbType.Boolean:
                    sysType = "AsBoolean()";
                    break;
                case DbType.Byte:
                    sysType = "AsByte()";
                    break;
                case DbType.Currency:
                    sysType = "AsCurrency()";
                    break;
                case DbType.Date:
                    sysType = "AsDate()";
                    break;
                case DbType.DateTime:
                    sysType = "AsDateTime()";
                    break;
                case DbType.Decimal:
                    sysType = column.IsCaracteristicsDefined() ? string.Format("AsDecimal({0}, {1})", column.Precision, column.Scale) : string.Format("AsDecimal(19, 5)");
                    break;
                case DbType.Double:
                    sysType = "AsDouble()";
                    break;
                case DbType.Guid:
                    sysType = "AsGuid()";
                    break;
                case DbType.Int16:
                case DbType.UInt16:
                    sysType = "AsInt16()";
                    break;
                case DbType.Int32:
                case DbType.UInt32:
                    sysType = "AsInt32()";
                    break;
                case DbType.Int64:
                case DbType.UInt64:
                    sysType = "AsInt64()";
                    break;
                case DbType.Single:
                    sysType = "AsFloat()";
                    break;
                case DbType.String:
                    sysType = string.Format("AsString({0})", sizeStr);
                    break;
                case DbType.StringFixedLength:
                    sysType = string.Format("AsFixedLengthString({0})", sizeStr);
                    break;
                default:
                    sysType = string.Empty;
                    break;
            }

            return sysType;
        }

        private static bool IncludeAction(SchemaAction actionsSource, SchemaAction includedAction)
        {
            return (actionsSource & includedAction) != SchemaAction.None;
        }
    }
}
